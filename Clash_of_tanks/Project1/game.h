#ifndef GAME_H
#define GAME_H

#include "simpleSdlWrapper/ebentoak.h"

/* MEZUAK */
#define ONGI_ETORRI_MEZUA "Sakatu return hasteko..."
#define GAME_OVER "img/gameOver.bmp"

/* PANTALLAK */
#define HASIERAKO_PANTALLA "img/pantallak/hasiera.bmp"
#define ENTRENAMENDUA_PANTALLA "img/pantallak/entrenamendua.bmp"
//#define BAKARKA_PANTALLA "img/pantallak/bakarka.bmp"
#define VERSUS_PANTALLA "img/pantallak/versus.bmp"
#define IRTEN_PANTALLA "img/pantallak/irten.bmp"
#define BUKAERA_PANTALLA "img/pantallak/end.bmp"
#define PANTALLA_ZURIA "img/pantallak/zuria.bmp"


/* SOINUA */
#define JOKOA_MAIN_SOUND "sound/BackgroundSound.wav"
#define JOKOA_CONFIRMBOX_SOUND "sound/confirm.wav"
#define JOKOA_SOUND "sound/terminator.wav" 


typedef enum { JOLASTEN, GALDU, IRABAZI }EGOERA;
typedef enum { IRUDIA, MARGOA, TESTUA } MOTA;


typedef struct {

    POSIZIOA pos;
    int id;
    MOTA mota;

} JOKO_ELEMENTUA;

int jokoaAurkeztu(void);
int Hasiera();
int amaiera(int egoera);
int bukaeraPantallaKargatu();
int kargatuPantalla(char pantalla[], int x, int y);
int Entrenamendua();
int Versus();
void reset();
void resetBullets();
void resetPlayers();
void resetButtons();
void resetImages();
void resetMaps();
void resetObstacles();

#endif