#ifndef BULLETS_H
#define BULLETS_H

#include "../simpleSdlWrapper/ebentoak.h"
#include <stdbool.h>

#define MAX_BULLETS 100
#define BULLET_SIZE 10
#define BULLET_IMAGE "img/bullets/ball.bmp"
#define BULLET_SPEED 5 // not being used yet
//#define NEW_BULLET 0
//#define OLD_BULLET 1

typedef enum BULLET_DIRECTION {NONE, ESKUMA , EZKERRA, GORA, BEHERA } BULLET_DIRECTION;
typedef enum BULLET_STATE {NEW_BULLET, OLD_BULLET, DESTROYED } BULLET_STATE;


typedef struct Bullet{

    int id;
    POSIZIOA pos;
    float speed;
    int status;
    POSIZIOA dpos;
    BULLET_DIRECTION direction;
    bool visible;
    int jokalaria;
}BULLET;

extern int bulletKop;
BULLET bullets[MAX_BULLETS];

int createBullet(int jokalaria);
void destroyBullet(int pos);
int getBulletPos(int id);
int updateBullets();
int checkBulletCollision(int bullet);
int checkCollisionWithPlayer(int bullet);
void printBullets();
void destroyBullet2(int id);

#endif