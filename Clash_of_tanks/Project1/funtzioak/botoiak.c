#include <stdio.h>

#include "SDL.h"

#include "botoiak.h"
#include "../simpleSdlWrapper/imagen.h"
#include "../simpleSdlWrapper/graphics.h"
#include "../simpleSdlWrapper/text.h"
//#include "application.h"

int botoiKop = 1;


void kargatuBotoia(char path[], char name[], int x, int y){

    int botoiaID = -1;

    botoiaID = irudiaKargatu(path);

    botoiak[botoiKop].id = botoiaID;

    botoiak[botoiKop].pos.x = x;
    botoiak[botoiKop].pos.y = y;
   
    strcpy(botoiak[botoiKop].name, name);
    botoiak[botoiKop].status = DEFAULT;

    irudiaMugitu(botoiak[botoiKop].id, botoiak[botoiKop].pos.x, botoiak[botoiKop].pos.y);

    pantailaGarbitu(255,255,255);
    irudiakMarraztu();
    pantailaBerriztu();

    botoiKop++;

}

void kargatuBotoia2(char path[], int x, int y){

    int botoiaID = -1;

    botoiaID = irudiaKargatu(path);

    botoiak[botoiKop].id = botoiaID;
    botoiak[botoiKop].pos.x = x;
    botoiak[botoiKop].pos.y = y;
    botoiak[botoiKop].status = DEFAULT;

    irudiaMugitu(botoiak[botoiKop].id, botoiak[botoiKop].pos.x, botoiak[botoiKop].pos.y);
    pantailaGarbitu(255,255,255);
    irudiakMarraztu();
    pantailaBerriztu();

    botoiKop++;

}

int aurkituBotoiaID(char name[]){

    int i = 0, botoiaID = -1;

    while (botoiaID == -1){
       
        if (!strcmp(botoiak[i].name, name)){
            
            botoiaID = botoiak[i].id;

        }

        i++;

    }

    // printf("Arkitu beharreko botoia: %s\n", name);
    // printf("BotoiarenID-a: %d\n", botoiaID);
    
    return botoiaID;

}

int handleButtonClick(POSIZIOA pos){

    int pantallaAldatu = 0, botoiaID = -1;;
    //BOTOIA botoia;

    //ENTRENAMENDUA BOTOIA
    if ((pos.x > BOTOIA_ENTRENAMENDUA_X && pos.x < BOTOIA_ENTRENAMENDUA_X + BOTOIA_WIDTH) && (pos.y > BOTOIA_ENTRENAMENDUA_Y && pos.y < BOTOIA_ENTRENAMENDUA_Y + BOTOIA_HEIGHT)){
        pantallaAldatu = 1;
    }

    //VERSUS BOTOIA
    if ((pos.x > BOTOIA_VERSUS_X && pos.x < BOTOIA_VERSUS_X + BOTOIA_WIDTH) && (pos.y > BOTOIA_VERSUS_Y && pos.y < BOTOIA_VERSUS_Y + BOTOIA_HEIGHT)){
        pantallaAldatu = 2;
    }

    //IRTEN BOTOIA
    if ((pos.x > BOTOIA_IRTEN_X && pos.x < BOTOIA_IRTEN_X + BOTOIA_WIDTH) && (pos.y > BOTOIA_IRTEN_Y && pos.y < BOTOIA_IRTEN_Y + BOTOIA_HEIGHT)){
        pantallaAldatu = 3;
    }

    printf("handleButtonClick: pantallaAldatu: %d\n", pantallaAldatu);

    return pantallaAldatu;

}

int handleButtonHover(POSIZIOA pos){

    int botoia = -1;

    //ENTRENAMENDUA BOTOIA
    if ((pos.x > 46 && pos.x < 249) && (pos.y > 240 && pos.y < 313)){
        botoia = aurkituBotoiaID("entrenamendua");
    }

    //VERSUS BOTOIA
    if ((pos.x > 46 && pos.x < 249) && (pos.y > 323 && pos.y < 396)){
        botoia = aurkituBotoiaID("versus");
    }

    //IRTEN BOTOIA
    if ((pos.x > 46 && pos.x < 249) && (pos.y > 406 && pos.y < 479)){
        botoia = aurkituBotoiaID("irten");
    }

    return botoia;

}

void chnageButtonStatus(int id){

    int status = -1;
    //char image[50];

    status = botoiak[id].status;

    switch (status) {

        case DEFAULT:

            botoiak[id].status = HOVER;
            
            // elementuaKendu(id);
            // kargatuBotoia(botoiak[id].img, botoiak[id].name);

        break;

        case HOVER:
            
            botoiak[id].status = DEFAULT;

        break;

        default:
        break;

    }    

}

void checkButtons(){

    int i;
    char image[50], name[50];

    for ( i = 0; i < botoiKop; i++){
        
        if (botoiak[i].status == HOVER){

            strcpy(botoiak[i].img, image);
            
            elementuaKendu(botoiak[i].id);

            if (!strcmp(image, BOTOIA_ENTRENAMENDUA)){
                kargatuBotoia(BOTOIA_ENTRENAMENDUA_HOVER, "entrenamendua", -1, -1);
            }
            else if (!strcmp(image, BOTOIA_VERSUS)) {
                kargatuBotoia(BOTOIA_VERSUS_HOVER, "versus", -1, -1);
            }
            else if (!strcmp(image, BOTOIA_IRTEN)){
                kargatuBotoia(BOTOIA_IRTEN_HOVER, "irten", -1, -1);
            }
            else if(!strcmp(image, BOTOIA_BOLUMENA)){
                kargatuBotoia(BOTOIA_IRTEN_HOVER, "bolumena", 660, 510);
            }

        }

    }
    

}

void changeButton(int id){

    POSIZIOA pos;
    char name[50], image[50];

    pos.x = botoiak[id].pos.x;
    pos.y = botoiak[id].pos.y;
    strcpy(botoiak[id].name, name);
    //choseButtonImage(id, image, name);
    
    elementuaKendu(id);
    kargatuBotoia(BOTOIA_ENTRENAMENDUA_HOVER, name, pos.x, pos.y);

}

void choseButtonImage(int id, char image[50], char name[50]){

    if (!strcmp(name, "entrenamendua")){
        strcpy(image, BOTOIA_ENTRENAMENDUA_HOVER);
    }
    else if (!strcmp(name, "versus")){
        strcpy(image, BOTOIA_VERSUS_HOVER);
    }
    else if (!strcmp(name, "irten")){
        strcpy(image, BOTOIA_IRTEN_HOVER);
    }
    else if (!strcmp(name, "bolumena")){
        strcpy(image, BOTOIA_BOLUMENA_HOVER);
    }
    // else if (!strcmp(name, "jarraitu")){
    //     strcpy(image, BOTOIA_JARRAITU_HOVER);
    // }

}

