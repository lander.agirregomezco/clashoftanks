#ifndef JOKALARIAK_H
#define JOKALARIAK_H

#include "../simpleSdlWrapper/imagen.h"
#include "../simpleSdlWrapper/ebentoak.h"

#define MAX_JOKALARIAK 50
#define JOKALARIA_SPEED 1.3
#define JOKALARIA_WIDTH 36
#define JOKALARIA_HEIGHT 36

#define JOKALARIA_WSAD 0
#define JOKALARIA_ARROWS 1

#define JOKALARIA_ESKUMA 1
#define JOKALARIA_EZKERRA 2
#define JOKALARIA_GORA 3
#define JOKALARIA_BEHERA 4

#define HASIERA_X_JOKALARIA1 292
#define HASIERA_Y_JOKALARIA1 302
#define HASIERA_X_JOKALARIA2 976
#define HASIERA_Y_JOKALARIA2 302

#define JOKALARIA1_IMAGE_EZKERRA "img/jokalariak/BerdeEzkerra.bmp"
#define JOKALARIA1_IMAGE_ESKUMA "img/jokalariak/BerdeEskuma.bmp"
#define JOKALARIA1_IMAGE_GORA "img/jokalariak/BerdeGora.bmp"
#define JOKALARIA1_IMAGE_BEHERA "img/jokalariak/BerdeBehera.bmp"

#define JOKALARIA2_IMAGE_EZKERRA "img/jokalariak/UrdinEzkerra.bmp"
#define JOKALARIA2_IMAGE_ESKUMA "img/jokalariak/UrdinEskuma.bmp"
#define JOKALARIA2_IMAGE_GORA "img/jokalariak/UrdinGora.bmp"
#define JOKALARIA2_IMAGE_BEHERA "img/jokalariak/UrdinBehera.bmp"

typedef struct JOKALARIA{

    int id;
    POSIZIOA pos;
    char img[50];
    int mugitzen;
    float speed;
    int health;

} JOKALARIA;

// JOKALARIA jokalaria1 = {.img = JOKALARIA1_IMAGE_ESKUMA};
// JOKALARIA jokalaria2 = {.img = JOKALARIA2_IMAGE_ESKUMA};

int jokalariaSortu(int x, int y);
void jokalariakSortu();
void jokalariaMugitu(int jokalaria, int direction);
int jokalariaImage(int jokalaria);
void resetPlayerPos(int jokalaria);
int checkPlayerCollision(int jokalaria);
//void checkCollisionWithBullet(int bullet);

JOKALARIA jokalariak[MAX_JOKALARIAK];
extern int jokalariKop;

#endif