#ifndef BOTOIAK_H
#define BOTOIAK_H

#include "../simpleSdlWrapper/ebentoak.h"

// BOTOIEN KOORDENATU ETA TAMAINAK
#define BOTOI_KOP_MAX 50
#define BOTOIA_WIDTH 203
#define BOTOIA_HEIGHT 73
#define BOTOIA_BACK_SIZE 120
#define BOTOIA_MARGIN 10
#define IRTEN_CONFIRM_BTN_WIDTH 152
#define IRTEN_CONFIRM_BTN_HEIGHT 55
#define IRTEN_BAI_BTN_X 401
#define IRTEN_BAI_BTN_Y 298
#define IRTEN_EZ_BTN_X 215
#define IRTEN_EZ_BTN_Y 298
#define BOTOIA_ENTRENAMENDUA_X 415
#define BOTOIA_ENTRENAMENDUA_Y 486
#define BOTOIA_VERSUS_X 681
#define BOTOIA_VERSUS_Y 486
#define BOTOIA_IRTEN_X 19
#define BOTOIA_IRTEN_Y 581
#define BOTOIA_BACK1_X 71
#define BOTOIA_BACK1_Y 497
#define BOTOIA_BACK2_X 1107
#define BOTOIA_BACK2_Y 497
#define BOTOIA_JARRAITU_X 1075
#define BOTOIA_JARRAITU_Y 581

// BOTOIEN IRUDIAK
#define BOTOIA_PLAY "img/buttons/play_button.bmp"
#define BOTOIA_ENTRENAMENDUA "img/buttons/training_btn.bmp"
#define BOTOIA_ENTRENAMENDUA_HOVER "img/buttons/training_btn_hover.bmp"
#define BOTOIA_VERSUS "img/buttons/versus_btn.bmp"
#define BOTOIA_VERSUS_HOVER "img/buttons/versus_btn_hover.bmp"
#define BOTOIA_IRTEN "img/buttons/irten_btn.bmp"
#define BOTOIA_IRTEN_HOVER "img/buttons/irten_btn_hover.bmp"
#define BOTOIA_BOLUMENA "img/buttons/volume_btn.bmp"
#define BOTOIA_BOLUMENA_HOVER "img/buttons/volume_btn_hover.bmp"
#define BOTOIA_MUTE "img/buttons/mute_btn.bmp"
#define BOTOIA_MUTE_HOVER "img/buttons/mute_btn_hover.bmp"
#define BOTOIA_JARRAITU "img/buttons/jarraitu_btn.bmp"
#define BOTOIA_JARRAITU_HOVER "img/buttons/jarraitu_btn_hover.bmp"

typedef enum{DEFAULT, HOVER} STATUS;

typedef struct{
    int id;
    POSIZIOA pos;
    char img[50];
    char name[50];
    STATUS status;
}BOTOIA;

extern int botoiKop;
BOTOIA botoiak[BOTOI_KOP_MAX];

void kargatuBotoia(char path[], char name[], int x, int y);
void kargatuBotoia2(char path[], int x, int y);
int aurkituBotoiaID(char name[]);
int handleButtonClick(POSIZIOA pos);
int handleButtonHover(POSIZIOA pos);
void chnageButtonStatus(int id);
void checkButtons();
void changeButton(int id);
void choseButtonImage(int id, char image[50], char name[50]);
//void kargatuBotoia3(BOTOIA* botoia);

#endif