#include <time.h>
#include "application.h"
#include "../simpleSdlWrapper/imagen.h"
#include "../simpleSdlWrapper/graphics.h"

int curtainKop = 0;

char progressbars[12][50] = {
    "img/progress_bar/loadingbar-1.bmp",
    "img/progress_bar/loadingbar-2.bmp",
    "img/progress_bar/loadingbar-3.bmp",
    "img/progress_bar/loadingbar-4.bmp",
    "img/progress_bar/loadingbar-5.bmp",
    "img/progress_bar/loadingbar-6.bmp",
    "img/progress_bar/loadingbar-7.bmp",
    "img/progress_bar/loadingbar-8.bmp",
    "img/progress_bar/loadingbar-9.bmp",
    "img/progress_bar/loadingbar-10.bmp",
    "img/progress_bar/loadingbar-11.bmp",
    "img/progress_bar/loadingbar-12.bmp"
};

void elementuaKendu(int id){

    irudiaKendu(id);
    pantailaGarbitu();
    irudiakMarraztu();
    pantailaBerriztu();

}

void progressBar() {

    int progressID = -1, pos = -1;
    int i = 1, lastTime = 0, currentTime;

    // Draw first progress bar image
    progressID = irudiaKargatu(progressbars[0]);
    irudiaMugitu(progressID, 488, 272);
    pantailaGarbitu();
    irudiakMarraztu();
    pantailaBerriztu();


    while (lastTime < 670){ // Duration of 670 miliseconds = 0,67 seconds

        currentTime = SDL_GetTicks();

        if (currentTime > lastTime + 50){ // Change image every 50 miliseconds = 0,05 seconds

            irudiaKendu(progressID);
            progressID = irudiaKargatu(progressbars[i]);
            irudiaMugitu(progressID, 488, 272);
            pantailaGarbitu();
            irudiakMarraztu();
            pantailaBerriztu();

            i++;

        }

        SDL_Delay(50); // We delay 50 miliseconds so we can see the progress bar grow

        lastTime = currentTime;

    }

    progressID = irudiaKargatu(progressbars[11]);
    irudiaMugitu(progressID, 488, 272);
    pantailaGarbitu();
    irudiakMarraztu();
    pantailaBerriztu();

}

void Curtains(int action){

    switch (action){

        case OPEN_CURTAINS:

            while (curtains[0].pos.y >= -276 && curtains[1].pos.y <= 563){
                openCurtains();
                irudiakMarraztu();
                pantailaBerriztu();
            }

        break;
    
        case CLOSE_CURTAINS:

            while (curtains[0].pos.y <= 0 && curtains[1].pos.y >= 388){
                closeCurtains();
                irudiakMarraztu();
                pantailaBerriztu();
            }

        break;

        default:
            break;
    }

}

void createCurtains(int action){

    switch (action){

        case OPEN_CURTAINS:
            createCurtain(PANTALLA_CURTAIN_TOP, 0, -276);
            createCurtain(PANTALLA_CURTAIN_BOTTOM, 0, 564);
        break;

        case CLOSE_CURTAINS:
            createCurtain(PANTALLA_CURTAIN_TOP, 0, 0);
            createCurtain(PANTALLA_CURTAIN_BOTTOM, 0, 338);
        break;

        default:
        break;

    }



}

void createCurtain(char image[], int x, int y){

    int id = -1;

    id = kargatuPantalla(image, x, y);

    curtains[curtainKop].id = id;       
    curtains[curtainKop].pos.x = x;
    curtains[curtainKop].pos.y = y;
    strcpy(curtains[curtainKop].img, image);

    //pantailaGarbitu();
    irudiakMarraztu();
    //pantailaBerriztu();

    curtainKop++;

}

void closeCurtains(){

    closeCurtain(0);
    closeCurtain(1);
   
}

void closeCurtain(int curtainNum){

    irudiaKendu(curtains[curtainNum].id);

    switch (curtainNum){

        case 0:
            curtains[curtainNum].id = irudiaKargatu(PANTALLA_CURTAIN_TOP);
            curtains[curtainNum].pos.y += 1.5;
            
        break;
    
        case 1:
            curtains[curtainNum].id = irudiaKargatu(PANTALLA_CURTAIN_BOTTOM);
            curtains[curtainNum].pos.y -= 1.5;

        break;

        default:
            break;
    }

    irudiaMugitu(curtains[curtainNum].id, curtains[curtainNum].pos.x, curtains[curtainNum].pos.y);

}

void openCurtains(){

    closeCurtain(0);
    closeCurtain(1);
   
}

void openCurtain(int curtainNum){

    irudiaKendu(curtains[curtainNum].id);

    switch (curtainNum){

        case 0:
            curtains[curtainNum].id = irudiaKargatu(PANTALLA_CURTAIN_TOP);
            curtains[curtainNum].pos.y -= 2;
            
        break;
    
        case 1:
            curtains[curtainNum].id = irudiaKargatu(PANTALLA_CURTAIN_BOTTOM);
            curtains[curtainNum].pos.y += 2;

        break;

        default:
            break;
    }

    irudiaMugitu(curtains[curtainNum].id, curtains[curtainNum].pos.x, curtains[curtainNum].pos.y);

}