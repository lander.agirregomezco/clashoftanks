#ifndef APPLICATION_H
#define APPLICATION_H

#include "../simpleSdlWrapper/ebentoak.h"

#define OPEN_CURTAINS 0
#define CLOSE_CURTAINS 1
#define CURTAIN_KOP 2
#define PANTALLA_CURTAIN_TOP "img/pantallak/curtain_top.bmp"
#define PANTALLA_CURTAIN_BOTTOM "img/pantallak/curtain_bottom.bmp"

typedef struct {
    int id;
    char img[50];
    POSIZIOA pos;
} CURTAIN;

extern int curtainKop;
CURTAIN curtains[CURTAIN_KOP];

extern char progressbars[12][50];

void elementuaKendu(int id);
void progressBar();
void Curtains(int action);
void createCurtains(int action);
void createCurtain(char image[], int x, int y);
void closeCurtains();
void closeCurtain(int curtainNum);
void openCurtains();
void openCurtain(int curtainNum);

#endif