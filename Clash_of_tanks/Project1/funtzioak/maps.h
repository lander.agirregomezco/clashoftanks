#ifndef MAPS_H
#define MAPS_H

#include "../simpleSdlWrapper/ebentoak.h"

#define MAP_ROWS 16
#define MAP_COLS 20

#define GROUND 0
#define BRICK 1
#define IRON 2
#define BASE 3

#define BASE_IMAGE "img/bases/base.bmp"
#define GROUND_IMAGE "img/pantallak/map_frame.bmp"
#define IRON_IMAGE "img/obstacles/iron.bmp"
#define BRICK_IMAGE "img/obstacles/brick.bmp"
#define BRICK_DESTROYED_IMAGE "img/obstacles/brick_destroyed.bmp"

#define OBSTACLE_SIZE 36
#define OBSTACLE_MAX_KOP 100

extern int map[MAP_ROWS][MAP_COLS];

typedef struct type{
    int id;
    int health;
    //POSIZIOA pos;
} TYPE;

typedef struct {
    int id;
    TYPE type;
    POSIZIOA pos;
} OBSTACLE;

OBSTACLE obstacles[OBSTACLE_MAX_KOP];
extern int obstacleKop;

int isReserved(int a, int b);
void createMapArray();
void createHolesInMapArray();
void generateMap();
void printMapArray();
void createObstacle(int type, int x, int y);
void deleteObstacle(int x, int y);
void destroyObstacle(int id);

#endif