#include "bullet.h"
#include "application.h"
#include "maps.h"
#include "../simpleSdlWrapper/imagen.h"
#include "../simpleSdlWrapper/graphics.h"
#include "jokalariak.h"
#include <windows.h>
#include <stdbool.h>

int bulletKop = 0;

int createBullet(int jokalaria) {

    int jokalariaCurrentImage = -1;

    if (bulletKop < 50) {

        bullets[bulletKop].id = irudiaKargatu(BULLET_IMAGE);

        // printf("----------- createBullet ---------\n");
        // printf("BulletPos: %d\n", bulletKop);
        // printf("BulletID: %d\n", bullets[bulletKop].id);

        jokalariaCurrentImage = jokalariaImage(jokalaria); // Tankearen zein irudi dagoen pantailan esaten digu (ezker, eskuma, gora edo behera)

        // Tankearen bala kañoiaren puntan jartzeko kalkuluak
        switch (jokalariaCurrentImage) {

            //ESKUMA
            case 1:
                bullets[bulletKop].pos.x = jokalariak[jokalaria].pos.x + 46;
                bullets[bulletKop].pos.y = jokalariak[jokalaria].pos.y + 13;
            break;

            //EZKERRA
            case 2:
                bullets[bulletKop].pos.x = jokalariak[jokalaria].pos.x - 10;
                bullets[bulletKop].pos.y = jokalariak[jokalaria].pos.y + 12;
            break;

            //GORA
            case 3:
                bullets[bulletKop].pos.x = jokalariak[jokalaria].pos.x + 12;
                bullets[bulletKop].pos.y = jokalariak[jokalaria].pos.y - 13;
            break;

            //BEHERA
            case 4:
                bullets[bulletKop].pos.x = jokalariak[jokalaria].pos.x + 12;
                bullets[bulletKop].pos.y = jokalariak[jokalaria].pos.y + 45;
            break;

        default:
            break;
        }

        bullets[bulletKop].dpos.x = BULLET_SPEED;
        bullets[bulletKop].dpos.y = BULLET_SPEED;
        bullets[bulletKop].status = NEW_BULLET;
        bullets[bulletKop].visible = true;
        bullets[bulletKop].jokalaria = jokalaria;

        // Guztia bere lekuan jarri eta pantallaratu
        irudiaMugitu(bullets[bulletKop].id, bullets[bulletKop].pos.x, bullets[bulletKop].pos.y);
        // pantailaGarbitu(255,255,255);
        // irudiakMarraztu();
        // pantailaBerriztu();

        bulletKop++; // Pantallan dauden bala kopuruari bat gehitu

    }

    return bullets[bulletKop - 1].id;

}

void printBullets() {

    int i;

    for (i = 0; i < bulletKop; i++) {

        printf("-------- BulletKop: %d -----------\n", i);
        printf("BulletID: %d\n", bullets[i].id);
        printf("BulletX: %d\n", bullets[i].pos.x);
        printf("BulletY: %d\n", bullets[i].pos.y);
        printf("Visible: %d\n", bullets[i].visible);
        printf("---------------------------------------\n");
    }


}

void destroyBullet(int id) {

    int pos;
    pos = getBulletPos(id);

    bullets[pos].pos.x = -BULLET_SIZE;
    bullets[pos].pos.y = -BULLET_SIZE;
    bullets[pos].status = DESTROYED;
    bullets[pos].dpos.x = 0;
    bullets[pos].dpos.y = 0;
    bullets[pos].direction = NONE;
    bullets[pos].visible = false;

}

int getBulletPos(int id) {

    int i = 0, aurkitu = -1;

    while (i < bulletKop && aurkitu == -1) {

        if (bullets[i].id == id) {

            aurkitu = i;

        }

        i++;

    }

    return aurkitu;

}

int updateBullets() {

    int i, direction = -1, egoera = -1;

    if (bulletKop > 0) {

        for (i = 0; i < bulletKop; i++) {

            if (!bullets[i].visible) {
                continue;
            }

            irudiaKendu(bullets[i].id);

            bullets[i].id = irudiaKargatu(BULLET_IMAGE);

            if (bullets[i].status == NEW_BULLET) {

                direction = jokalariaImage(bullets[i].jokalaria); // Get tanks current image (either top, bottom, right, left

                bullets[i].direction = direction;
                bullets[i].status = OLD_BULLET;

            }

            // Decide which direction should the ball take
            switch (bullets[i].direction) {

            case ESKUMA:

                bullets[i].pos.x += bullets[i].dpos.x;

                break;

            case EZKERRA:

                bullets[i].pos.x -= bullets[i].dpos.x;

                break;

            case GORA:

                bullets[i].pos.y -= bullets[i].dpos.y;

                break;

            case BEHERA:

                bullets[i].pos.y += bullets[i].dpos.y;

                break;

            default:
                break;

            }

            if (bullets[i].pos.x + BULLET_SIZE > 1009 || bullets[i].pos.x - BULLET_SIZE < 254 || checkCollisionWithObject(i) == 1) {

                bullets[i].dpos.x *= -1;

            }

            if (bullets[i].pos.y - BULLET_SIZE < 35 || bullets[i].pos.y + BULLET_SIZE > 626 || checkCollisionWithObject(i) == 1) {
                bullets[i].dpos.y *= -1;
            }

            if (checkCollisionWithPlayer(i) == 1) { // End game
                //destroyBullet(bullets[i].id);
                egoera = 0;
                break;
            }
            else { // Take off one life of player
                //destroyBullet(bullets[i].id);
            }

            irudiaMugitu(bullets[i].id, bullets[i].pos.x, bullets[i].pos.y);

        }

    }

    return egoera;

}

int checkCollisionWithObject(int bullet) {

    int i = 0, collision = 0;

    while (i < obstacleKop && collision == 0) {

        if (!(bullets[bullet].pos.x > obstacles[i].pos.x + OBSTACLE_SIZE
            || bullets[bullet].pos.x + BULLET_SIZE < obstacles[i].pos.x
            || bullets[bullet].pos.y > obstacles[i].pos.y + OBSTACLE_SIZE
            || bullets[bullet].pos.y + BULLET_SIZE < obstacles[i].pos.y)) {

             if (obstacles[i].type.id == BRICK){
                 deleteObstacle(obstacles[i].pos.x, obstacles[i].pos.y);
                 destroyObstacle(obstacles[i].id);
                 destroyBullet(bullets[bullet].id);
                 collision = 1;
             }
             else {
                 collision = 1;
             }

        }

        i++;

    }

    return collision;

}

int checkCollisionWithPlayer(int bullet) {

    int i = 0, collision = 0;

    while (i < jokalariKop && collision == 0) {

        if (bullets[bullet].pos.x - BULLET_SIZE > jokalariak[i].pos.x
            && bullets[bullet].pos.x + BULLET_SIZE < jokalariak[i].pos.x + JOKALARIA_WIDTH
            && bullets[bullet].pos.y + BULLET_SIZE > jokalariak[i].pos.y
            && bullets[bullet].pos.y - BULLET_SIZE < jokalariak[i].pos.y + JOKALARIA_HEIGHT) {

            if (jokalariak[i].health != 0){
                jokalariak[i].health -= 1;
                destroyBullet(bullets[bullet].id);
                //generateMap();
                resetPlayerPos(JOKALARIA_WSAD);
                resetPlayerPos(JOKALARIA_ARROWS);
                //irudiaMugitu(jokalariak[JOKALARIA_WSAD].id, HASIERA_X_JOKALARIA1, HASIERA_Y_JOKALARIA1);
                //irudiaMugitu(jokalariak[JOKALARIA_ARROWS].id, HASIERA_X_JOKALARIA2, HASIERA_Y_JOKALARIA2);
                collision = 2;
            }
            else {
                collision = 1;
            }

        }

        i++;

    }

    //printf("%d\n", collision);

    return collision;

}