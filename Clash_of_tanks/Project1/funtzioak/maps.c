#include <time.h>
#include "maps.h"
#include "application.h"
#include "../simpleSdlWrapper/imagen.h"

int map[MAP_ROWS][MAP_COLS];

typedef struct {
    POSIZIOA pos;
} RESERVED;

RESERVED reserved[10] = { {6, 0}, {6, 1}, {7, 1}, {8, 1}, {8, 0}, {7, 19}, {7, 18}, {8, 18}, {9, 18}, {9, 19} };

int obstacleKop = 0;

void createMapArray() {

    int i, j;

    for (i = 0; i < MAP_ROWS; i++) {

        for (j = 0; j < MAP_COLS; j++) {

            if ((i == 7 && j == 0) || (i == 7 && j == 19)){
                map[i][j] = 3;
            }
            else {
                map[i][j] = 0;
            }

        }

    }

}

int isReserved(int a, int b) {

    int i = 0, res = 0;

    while (i < 10 && res == 0) {

        if (a == reserved[i].pos.x && b == reserved[i].pos.y) {
            res = 1;
        }

        i++;

    }

    return res;

}

void createHolesInMapArray() {

    int i, col, n, row;
    int types[10] = { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2 };
    int iron = 0, brick = 0, total = 0;

    srand(time(0));

    for (row = 0; row < MAP_ROWS; row++) {

        n = rand() % ((16 - 15) + 3);

        for (i = 0; i < n; i++) {

            col = rand() % MAP_COLS;

            if (map[row][col] != 3 && isReserved(row, col) != 1){

                n = rand() % 9;
                map[row][col] = types[n];

            }

        }

    }

}

void printMapArray(){

    int i, j;

    for (i = 0; i < MAP_ROWS; i++) {

        for (j = 0; j < MAP_COLS; j++) {

            printf("%d  ", map[i][j]);

        }
        printf("\n");
    }
    
}

void generateMap(){

    int i, j, x, y;

    createMapArray();
    createHolesInMapArray();

    for (i = 0; i < MAP_ROWS; i++){
        
        for (j = 0; j < MAP_COLS; j++){
            
            x = (OBSTACLE_SIZE * j) + 292;
            y = (OBSTACLE_SIZE * i) + 50;

            if (map[i][j] != GROUND){

                //if (map[i][j] == 3)
                //{
                //    printf("x: %d\n", x);
                //    printf("y: %d\n", y);
                //}

                createObstacle(map[i][j], x, y);
            }
            
        }
        
    }

}

void createObstacle(int type, int x, int y) {

    switch (type){

        case BRICK:
            obstacles[obstacleKop].id = irudiaKargatu(BRICK_IMAGE);
            obstacles[obstacleKop].type.id = BRICK;
        break;

        case IRON:
            obstacles[obstacleKop].id = irudiaKargatu(IRON_IMAGE);
            obstacles[obstacleKop].type.id = IRON;
        break;

        case BASE:
            obstacles[obstacleKop].id = irudiaKargatu(BASE_IMAGE);
            obstacles[obstacleKop].type.id = BASE;
        break;

    }

    //if (type == BRICK) {
    //    obstacles[obstacleKop].id = irudiaKargatu(BRICK_IMAGE);
    //    obstacles[obstacleKop].type.id = BRICK;
    //}
    //else if (type == IRON) {
    //    obstacles[obstacleKop].id = irudiaKargatu(IRON_IMAGE);
    //    obstacles[obstacleKop].type.id = IRON;
    //}

    obstacles[obstacleKop].pos.x = x;
    obstacles[obstacleKop].pos.y = y;

    irudiaMugitu(obstacles[obstacleKop].id, obstacles[obstacleKop].pos.x, obstacles[obstacleKop].pos.y);

    obstacleKop++;

}

void deleteObstacle(int x, int y) {

    int i, j;

    i = x / OBSTACLE_SIZE;
    j = y / OBSTACLE_SIZE;

    map[i][j] = 0;

}

int getObstaclePos(int id) {

    int i = 0, aurkitu = -1;

    while (i < obstacleKop && aurkitu == -1) {

        if (obstacles[i].id == id) {

            aurkitu = i;

        }

        i++;

    }

    return aurkitu;

}

void destroyObstacle(int id) {

    int pos;
    pos = getObstaclePos(id);

    obstacles[pos].pos.x = -OBSTACLE_SIZE;
    obstacles[pos].pos.y = -OBSTACLE_SIZE;
    obstacles[pos].type.health = -1;

    irudiaMugitu(obstacles[pos].id, obstacles[pos].pos.x, obstacles[pos].pos.y);

}