#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

#include "SDL.h"

#include "jokalariak.h"
#include "bullet.h"
#include "../simpleSdlWrapper/graphics.h"
#include "../simpleSdlWrapper/text.h"
#include "../simpleSdlWrapper/imagen.h"
#include "maps.h"
#include <windows.h>

int jokalariKop = 0;

// Jokalaria sortzen du
int jokalariaSortu(int x, int y){

    int jokalariaID = -1;
    char image[50];

    // Default image direction
    switch (jokalariKop){

        case 0:
            printf("Jokalaria1\n");
            strcpy(image, JOKALARIA1_IMAGE_ESKUMA);
        break;

        case 1:
            printf("Jokalaria2\n");
            strcpy(image, JOKALARIA2_IMAGE_EZKERRA);
        break;
        
        default:
            break;
    }

    jokalariaID = irudiaKargatu(image);

    jokalariak[jokalariKop].id = jokalariaID;
    jokalariak[jokalariKop].pos.x = x;
    jokalariak[jokalariKop].pos.y = y;
    jokalariak[jokalariKop].mugitzen = 0;
    jokalariak[jokalariKop].speed = 1.3;
    jokalariak[jokalariKop].health = 2;

    irudiaMugitu(jokalariak[jokalariKop].id, jokalariak[jokalariKop].pos.x, jokalariak[jokalariKop].pos.y);

    jokalariKop++;

    return jokalariaID;

}

// Tanke irudia pantallan sartzeko prestatzen du eta pantallan sartzen du
void jokalariakSortu(){

    jokalariaSortu(HASIERA_X_JOKALARIA1, HASIERA_Y_JOKALARIA1);
    jokalariaSortu(HASIERA_X_JOKALARIA2, HASIERA_Y_JOKALARIA2);
    
    pantailaGarbitu();
    irudiakMarraztu();
    pantailaBerriztu();

}

void jokalariaMugitu(int jokalaria, int direction){

    int x, y;

    x =  jokalariak[jokalaria].pos.x;
    y =  jokalariak[jokalaria].pos.y;

    irudiaKendu(jokalariak[jokalaria].id);

    jokalariak[jokalaria].speed = 1.3;

    if (jokalaria == JOKALARIA_WSAD){

        switch (direction){
            
            case JOKALARIA_ESKUMA:
                jokalariak[jokalaria].id = irudiaKargatu(JOKALARIA1_IMAGE_ESKUMA);
                strcpy(jokalariak[jokalaria].img, JOKALARIA1_IMAGE_ESKUMA);
                jokalariak[jokalaria].pos.x += jokalariak[jokalaria].speed + 1;
            break;

            case JOKALARIA_EZKERRA:
                jokalariak[jokalaria].id = irudiaKargatu(JOKALARIA1_IMAGE_EZKERRA);
                strcpy(jokalariak[jokalaria].img, JOKALARIA1_IMAGE_EZKERRA);
                jokalariak[jokalaria].pos.x -= jokalariak[jokalaria].speed;
            break;

            case JOKALARIA_GORA:
                jokalariak[jokalaria].id = irudiaKargatu(JOKALARIA1_IMAGE_GORA);
                strcpy(jokalariak[jokalaria].img, JOKALARIA1_IMAGE_GORA);
                jokalariak[jokalaria].pos.y -= jokalariak[jokalaria].speed;
            break;

            case JOKALARIA_BEHERA:
                jokalariak[jokalaria].id = irudiaKargatu(JOKALARIA1_IMAGE_BEHERA);
                strcpy(jokalariak[jokalaria].img, JOKALARIA1_IMAGE_BEHERA);
                jokalariak[jokalaria].pos.y += jokalariak[jokalaria].speed + 1;
            break;
            
            default:
            break;

        }

    }
    else if (jokalaria == JOKALARIA_ARROWS){
        
        switch (direction){
            
            case JOKALARIA_ESKUMA:
                jokalariak[jokalaria].id = irudiaKargatu(JOKALARIA2_IMAGE_ESKUMA);
                strcpy(jokalariak[jokalaria].img, JOKALARIA2_IMAGE_ESKUMA);
                jokalariak[jokalaria].pos.x += jokalariak[jokalaria].speed + 1;
            break;

            case JOKALARIA_EZKERRA:
                jokalariak[jokalaria].id = irudiaKargatu(JOKALARIA2_IMAGE_EZKERRA);
                strcpy(jokalariak[jokalaria].img, JOKALARIA2_IMAGE_EZKERRA);
                jokalariak[jokalaria].pos.x -= jokalariak[jokalaria].speed;
            break;

            case JOKALARIA_GORA:
                jokalariak[jokalaria].id = irudiaKargatu(JOKALARIA2_IMAGE_GORA);
                strcpy(jokalariak[jokalaria].img, JOKALARIA2_IMAGE_GORA);
                jokalariak[jokalaria].pos.y -= jokalariak[jokalaria].speed;
            break;

            case JOKALARIA_BEHERA:
                jokalariak[jokalaria].id = irudiaKargatu(JOKALARIA2_IMAGE_BEHERA);
                strcpy(jokalariak[jokalaria].img, JOKALARIA2_IMAGE_BEHERA);
                jokalariak[jokalaria].pos.y += jokalariak[jokalaria].speed + 1;
            break;
            
            default:
            break;

        }

    }
    
    if (x < 290 || (x + JOKALARIA_WIDTH > 1013) || y < 60 || y + JOKALARIA_HEIGHT > 626 || checkPlayerCollision(jokalaria) == 1){

        switch (direction){

            case JOKALARIA_ESKUMA:
                jokalariak[jokalaria].pos.x -= jokalariak[jokalaria].speed + JOKALARIA_WIDTH/2;
            break;

            case JOKALARIA_EZKERRA:
                jokalariak[jokalaria].pos.x += jokalariak[jokalaria].speed + JOKALARIA_WIDTH/2;
            break;

            case JOKALARIA_GORA:
                jokalariak[jokalaria].pos.y += jokalariak[jokalaria].speed + JOKALARIA_HEIGHT/2;
            break;

            case JOKALARIA_BEHERA:
                jokalariak[jokalaria].pos.y -= jokalariak[jokalaria].speed + JOKALARIA_HEIGHT/2;
            break;
        
            default:
            break;

        }
        
    }

    irudiaMugitu(jokalariak[jokalaria].id, jokalariak[jokalaria].pos.x, jokalariak[jokalaria].pos.y);

}

void resetPlayerPos(int jokalaria) {

    switch (jokalaria){

        case JOKALARIA_WSAD:
            jokalariak[jokalaria].pos.x = HASIERA_X_JOKALARIA1;
            jokalariak[jokalaria].pos.y = HASIERA_Y_JOKALARIA1;
            strcpy(jokalariak[jokalaria].img, JOKALARIA1_IMAGE_ESKUMA);
            irudiaMugitu(jokalariak[jokalaria].id, HASIERA_X_JOKALARIA1, HASIERA_Y_JOKALARIA1);
        break;

        case JOKALARIA_ARROWS:
            jokalariak[jokalaria].pos.x = HASIERA_X_JOKALARIA2;
            jokalariak[jokalaria].pos.y = HASIERA_Y_JOKALARIA2;
            strcpy(jokalariak[jokalaria].img, JOKALARIA2_IMAGE_ESKUMA);
            irudiaMugitu(jokalariak[jokalaria].id, HASIERA_X_JOKALARIA2, HASIERA_Y_JOKALARIA2);
        break;

        default:
        break;
    }

}

// Tankearen zein irudi dagoen pantalla esaten du
int jokalariaImage(int jokalaria){

    int img = 1;

    switch (jokalaria){

        case JOKALARIA_WSAD:

            if (!strcmp(jokalariak[jokalaria].img, JOKALARIA1_IMAGE_ESKUMA)){
                img = 1;
            }
            
            if (!strcmp(jokalariak[jokalaria].img, JOKALARIA1_IMAGE_EZKERRA)){
                img = 2;
            }

            if (!strcmp(jokalariak[jokalaria].img, JOKALARIA1_IMAGE_GORA)){
                img = 3;
            }

            if (!strcmp(jokalariak[jokalaria].img, JOKALARIA1_IMAGE_BEHERA)){
                img = 4;
            }

        break;

        case JOKALARIA_ARROWS:

            if (!strcmp(jokalariak[jokalaria].img, JOKALARIA2_IMAGE_ESKUMA)){
                img = 1;
            }
            
            if (!strcmp(jokalariak[jokalaria].img, JOKALARIA2_IMAGE_EZKERRA)){
                img = 2;
            }

            if (!strcmp(jokalariak[jokalaria].img, JOKALARIA2_IMAGE_GORA)){
                img = 3;
            }

            if (!strcmp(jokalariak[jokalaria].img, JOKALARIA2_IMAGE_BEHERA)){
                img = 4;
            }

        break;
        
        default:
            break;
    }

    return img;

}

int checkPlayerCollision(int jokalaria){

    int i = 0, collision = 0;

    while (i < obstacleKop && collision == 0){

        if (obstacles[i].type.id != 3){

            if (!(jokalariak[jokalaria].pos.x > obstacles[i].pos.x + OBSTACLE_SIZE
                || jokalariak[jokalaria].pos.x + JOKALARIA_WIDTH < obstacles[i].pos.x
                || jokalariak[jokalaria].pos.y > obstacles[i].pos.y + OBSTACLE_SIZE
                || jokalariak[jokalaria].pos.y + JOKALARIA_HEIGHT < obstacles[i].pos.y)) {

                collision = 1;

            }

        }
        
        i++;

    }

    return collision;

}




