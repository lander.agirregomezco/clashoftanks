#define _CRT_SECURE_NO_WARNINGS

#include "game.h"
#include "simpleSdlWrapper/imagen.h"
#include "simpleSdlWrapper/graphics.h"
#include "simpleSdlWrapper/ebentoak.h"
#include "simpleSdlWrapper/text.h"
#include "simpleSdlWrapper/soinua.h"
#include "funtzioak/botoiak.h"
#include "funtzioak/jokalariak.h"
#include "funtzioak/bullet.h"
#include "funtzioak/application.h"
#include "funtzioak/maps.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <windows.h>

void hasi() {

    int ebentu = 0;
    POSIZIOA pos;

    createCurtains(CLOSE_CURTAINS);

    progressBar();

    kargatuBotoia(BOTOIA_PLAY, "play", 616, 364);

    do
    {

        ebentu = ebentuaJasoGertatuBada();

        if (ebentu == SAGU_BOTOIA_EZKERRA){

            pos = saguarenPosizioa();
            if ((pos.x > 616 && pos.x < 616 + 63) && (pos.y > 364 && pos.y < 364 + 76)){

                ebentu = 1;

            }

        }

    } while (ebentu != 1);

    pantailaGarbitu();
    pantailaBerriztu();

}

int jokoaAurkeztu(void) {

    int i = 0, ebentu = 0, pantallaID = -1, pantallaAldatu = 0;
    POSIZIOA pos;

    //textuaGaitu();
    pantallaID = Hasiera();

    audioInit();
    loadTheMusic(JOKOA_MAIN_SOUND);
    playMusic();

    audioInit();

    do {

        ebentu = ebentuaJasoGertatuBada();

        if (ebentu == SAGU_BOTOIA_EZKERRA)
        {

            pos = saguarenPosizioa();
            pantallaAldatu = handleButtonClick(pos);

            switch (pantallaAldatu)
            {

                case 1:
                    pantallaID = kargatuPantalla(ENTRENAMENDUA_PANTALLA, 0, 0);
                    kargatuBotoia(BOTOIA_JARRAITU, "jarraitu", 1075, 581);
                    pantallaAldatu = 0;
                    do{

                        ebentu = ebentuaJasoGertatuBada();

                        if (ebentu == SAGU_BOTOIA_EZKERRA){

                            pos = saguarenPosizioa();

                            if ((pos.x > BOTOIA_JARRAITU_X && pos.x < BOTOIA_JARRAITU_X + BOTOIA_WIDTH) && (pos.y > BOTOIA_JARRAITU_Y && pos.y < BOTOIA_JARRAITU_Y + BOTOIA_HEIGHT)){
                                pantallaAldatu = 1;
                            }

                        }

                    } while (pantallaAldatu == 0);

                    //Curtains(CLOSE_CURTAINS);
                    //SDL_Delay(1500);
                    Entrenamendua();

                    break;

                case 2:
                    pantallaID = kargatuPantalla(VERSUS_PANTALLA, 0, 0);
                    kargatuBotoia(BOTOIA_JARRAITU, "jarraitu", 1075, 581);
                    pantallaAldatu = 0;
                    do {

                        ebentu = ebentuaJasoGertatuBada();

                        if (ebentu == SAGU_BOTOIA_EZKERRA) {

                            pos = saguarenPosizioa();

                            if ((pos.x > BOTOIA_JARRAITU_X && pos.x < BOTOIA_JARRAITU_X + BOTOIA_WIDTH) && (pos.y > BOTOIA_JARRAITU_Y && pos.y < BOTOIA_JARRAITU_Y + BOTOIA_HEIGHT)) {
                                pantallaAldatu = 1;
                            }

                        }

                    } while (pantallaAldatu == 0);

                    //Curtains(CLOSE_CURTAINS);
                    //SDL_Delay(1500);
                    Versus();
                break;
                
                default:
                break;

            }

        }

    } while (ebentu != TECLA_ESCAPE && pantallaAldatu == 0); // Either the player hit ESC key or clicked in one of the playing btns

    //printf("PantallaAldatu: %d\n", pantallaAldatu);

    return pantallaAldatu;

}

int amaiera(int egoera) {
    int ebentu = 0, id, ret = -1;
    int idAudioGame;
    POSIZIOA pos;

    idAudioGame = loadSound(JOKOA_SOUND);
    playSound(idAudioGame);

    id = bukaeraPantallaKargatu(egoera);
    do{

        ebentu = ebentuaJasoGertatuBada();

        if (ebentu == SAGU_BOTOIA_EZKERRA){

            pos = saguarenPosizioa();

            if ((pos.x > 481 && pos.x < 481 + 152) && (pos.y > 347 && pos.y < 347 + 55)) {

                ret = 1;
                reset();

            }

            if ((pos.x > 667 && pos.x < 667 + 152) && (pos.y > 347 && pos.y < 347 + 55)) {

                ret = 0;

            }

        }

    } while (ret == -1);

    audioTerminate();

    return ret;
}

int bukaeraPantallaKargatu(int egoera) {
    int id = -1;

    if (egoera == 3){
        id = irudiaKargatu(IRTEN_PANTALLA);
    }
    else {
        id = irudiaKargatu(BUKAERA_PANTALLA);
    }

    irudiaMugitu(id, 0, 0);
    pantailaGarbitu();
    irudiakMarraztu();
    pantailaBerriztu();
    return id;
}

int Hasiera() {

    int pantallaID = -1;

    pantallaID = kargatuPantalla(HASIERAKO_PANTALLA, 0, 0);

    kargatuBotoia(BOTOIA_ENTRENAMENDUA, "entrenamendua", BOTOIA_ENTRENAMENDUA_X, BOTOIA_ENTRENAMENDUA_Y);
    kargatuBotoia(BOTOIA_VERSUS, "versus", BOTOIA_VERSUS_X, BOTOIA_VERSUS_Y);
    kargatuBotoia(BOTOIA_IRTEN, "irten", BOTOIA_IRTEN_X, BOTOIA_IRTEN_Y);

    return pantallaID;

}

int Entrenamendua() {

    int ebentu = 0, balaID = -1, pantallaID = -1, egoera = -1;
    POSIZIOA pos;

    pantallaID = kargatuPantalla(GROUND_IMAGE, 0, 0);

    generateMap();

    jokalariaSortu(292, 302);

    pantailaGarbitu();
    irudiakMarraztu();
    pantailaBerriztu();

    const Uint8* keystates = SDL_GetKeyboardState(NULL);

    do {

        ebentu = ebentuaJasoGertatuBada();

        if (keystates[SDL_SCANCODE_A]) {
            jokalariaMugitu(JOKALARIA_WSAD, JOKALARIA_EZKERRA);
        }

        if (keystates[SDL_SCANCODE_D]) {
            jokalariaMugitu(JOKALARIA_WSAD, JOKALARIA_ESKUMA);
        }

        if (keystates[SDL_SCANCODE_W]) {
            jokalariaMugitu(JOKALARIA_WSAD, JOKALARIA_GORA);
        }

        if (keystates[SDL_SCANCODE_S]) {
            jokalariaMugitu(JOKALARIA_WSAD, JOKALARIA_BEHERA);
        }

        if (ebentu == TECLA_SPACE) {

            balaID = createBullet(JOKALARIA_WSAD);

        }

        if (ebentu == SAGU_BOTOIA_EZKERRA) {

            pos = saguarenPosizioa();

            printf("PosX: %d\n", pos.x);
            printf("PosY: %d\n", pos.y);

            // Check if any of the back buttons were clicked
            if ((pos.x > BOTOIA_BACK1_X && pos.x < BOTOIA_BACK1_X + BOTOIA_BACK_SIZE) && (pos.y > BOTOIA_BACK1_Y && pos.y < BOTOIA_BACK1_Y + BOTOIA_BACK_SIZE))
            {
                egoera = 0;
                printf("back1");
            }

            if ((pos.x > BOTOIA_BACK2_X && pos.x < BOTOIA_BACK2_X + BOTOIA_BACK_SIZE) && (pos.y > BOTOIA_BACK2_Y && pos.y < BOTOIA_BACK2_Y + BOTOIA_BACK_SIZE))
            {
                egoera = 0;
                printf("back2");
            }

        }

        updateBullets();
        irudiakMarraztu();
        pantailaBerriztu();
        Sleep(25);

    } while (ebentu != TECLA_ESCAPE && egoera == -1);


    return 0;

}

int Versus() {

    int ebentu = 0, balaID = -1, pantallaID = -1, egoera = -1;
    POSIZIOA pos;

    char health1[3], health2[3];

    pantallaID = kargatuPantalla(GROUND_IMAGE, 0, 0);

    generateMap();
    printMapArray();

    jokalariakSortu();

    textuaGaitu(64);

    do {

        ebentu = ebentuaJasoGertatuBada();

        const Uint8* keystates = SDL_GetKeyboardState(NULL);

        if (keystates[SDL_SCANCODE_A]) {
            jokalariaMugitu(JOKALARIA_WSAD, JOKALARIA_EZKERRA);
        }

        if (keystates[SDL_SCANCODE_D]) {
            jokalariaMugitu(JOKALARIA_WSAD, JOKALARIA_ESKUMA);
        }

        if (keystates[SDL_SCANCODE_W]) {
            jokalariaMugitu(JOKALARIA_WSAD, JOKALARIA_GORA);
        }

        if (keystates[SDL_SCANCODE_S]) {
            jokalariaMugitu(JOKALARIA_WSAD, JOKALARIA_BEHERA);
        }

        if (keystates[SDL_SCANCODE_LEFT]) {
            jokalariaMugitu(JOKALARIA_ARROWS, JOKALARIA_EZKERRA);
        }

        if (keystates[SDL_SCANCODE_RIGHT]) {
            jokalariaMugitu(JOKALARIA_ARROWS, JOKALARIA_ESKUMA);
        }

        if (keystates[SDL_SCANCODE_UP]) {
            jokalariaMugitu(JOKALARIA_ARROWS, JOKALARIA_GORA);
        }

        if (keystates[SDL_SCANCODE_DOWN]) {
            jokalariaMugitu(JOKALARIA_ARROWS, JOKALARIA_BEHERA);
        }

        if (ebentu == TECLA_c) {
            balaID = createBullet(JOKALARIA_WSAD);
        }

        if (ebentu == TECLA_SPACE) {
            balaID = createBullet(JOKALARIA_ARROWS);
        }

        if (ebentu == SAGU_BOTOIA_EZKERRA){

            pos = saguarenPosizioa();

            // Check if any of the back buttons were clicked
            if ((pos.x > BOTOIA_BACK1_X && pos.x < BOTOIA_BACK1_X + BOTOIA_BACK_SIZE) && (pos.y > BOTOIA_BACK1_Y && pos.y < BOTOIA_BACK1_Y + BOTOIA_BACK_SIZE))
            {
                egoera = 0;
            }

            if ((pos.x > BOTOIA_BACK2_X && pos.x < BOTOIA_BACK2_X + BOTOIA_BACK_SIZE) && (pos.y > BOTOIA_BACK2_Y && pos.y < BOTOIA_BACK2_Y + BOTOIA_BACK_SIZE))
            {
                egoera = 0;
            }

        }

        //pantailaBerriztu();
        pantailaGarbitu();
        irudiakMarraztu();
        if (jokalariak[JOKALARIA_WSAD].health >= 0)
        {
            textuaIdatzi(113, 237, _itoa(jokalariak[JOKALARIA_WSAD].health, health1, 10));

        }
        if (jokalariak[JOKALARIA_ARROWS].health >= 0)
        {
            textuaIdatzi(1149, 237, _itoa(jokalariak[JOKALARIA_ARROWS].health, health2, 10));

        }
        
        pantailaBerriztu();
        Sleep(15);

        if (egoera == -1) {
            egoera = updateBullets();
        }

    } while (ebentu != TECLA_ESCAPE && egoera == -1);

    return egoera;

}

int Irten() {

    int pantalla = -1, ebentu = -1;
    POSIZIOA pos;

    pantalla = kargatuPantalla(IRTEN_PANTALLA, 0, 0);
    loadTheMusic(JOKOA_CONFIRMBOX_SOUND);
    playMusic();
    SDL_Delay(260);
    audioTerminate();

    do { // We wait till the player decides to confirm or decline exiting the game

        ebentu = ebentuaJasoGertatuBada();

        if (ebentu == SAGU_BOTOIA_EZKERRA) {
            pos = saguarenPosizioa();
        }

    } while (ebentu != SAGU_BOTOIA_EZKERRA);

    // The player decided to stay a little more, yay! Let's get him back to the homepage
    if ((pos.x > IRTEN_EZ_BTN_X && pos.x < IRTEN_EZ_BTN_X + IRTEN_CONFIRM_BTN_WIDTH) && (pos.y > IRTEN_EZ_BTN_Y && pos.y < IRTEN_EZ_BTN_Y + IRTEN_CONFIRM_BTN_HEIGHT)) {
        pantalla = 0;
    }
    else {
        pantalla = 1;
    }

    return pantalla;

}

int kargatuPantalla(char pantalla[], int x, int y) {

    int pantallaID = -1;

    pantallaID = irudiaKargatu(pantalla);

    irudiaMugitu(pantallaID, x, y);
    pantailaGarbitu();
    irudiakMarraztu();
    pantailaBerriztu();

    return pantallaID;
}

void reset() {

    resetBullets();
    resetPlayers();
    resetButtons();
    resetImages();
    resetObstacles();

}

void resetBullets() {

    int i;

    for (i = 0; i < bulletKop; i++) {

        bullets[i].id = 0;
        bullets[i].pos.x = 0;
        bullets[i].pos.y = 0;
        bullets[i].status = 0;
        bullets[i].dpos.x = 0;
        bullets[i].dpos.y = 0;
        bullets[i].direction = 0;
        bullets[i].visible = false;
        bullets[i].jokalaria = 0;

    }

    bulletKop = 0;

}

void resetPlayers() {

    int i;

    for (i = 0; i < jokalariKop; i++) {

        jokalariak[i].id = 0;
        jokalariak[i].pos.x = 0;
        jokalariak[i].pos.y = 0;
        strcpy(jokalariak[i].img, "\0");
        jokalariak[i].speed = 0;
        jokalariak[i].health = 3;

    }

    jokalariKop = 0;

}

void resetButtons() {

    int i;

    for (i = 0; i < botoiKop; i++) {

        botoiak[i].id = 0;
        botoiak[i].pos.x = 0;
        botoiak[i].pos.y = 0;
        strcpy(botoiak[i].img, "\0");
        strcpy(botoiak[i].name, "\0");
        botoiak[i].status = 0;

    }

    botoiKop = 0;

}

void resetImages() {

    int i, kop = irudiKop;

    for (i = 0; i < kop; i++) {

        irudiaKendu(irudiak[i].id);

    }

}

void resetMaps() {

    int i, j;

    for (i = 0; i < MAP_ROWS; i++) {

        for (j = 0; j < MAP_COLS; j++) {

            if ((i == 7 && j == 0) || (i == 7 && j == 19)) {
                map[i][j] = 3;
            }
            else {
                map[i][j] = 0;
            }

        }

    }

}

void resetObstacles() {

    int i;

    for  (i = 0; i < obstacleKop; i++){

        destroyObstacle(obstacles[i].id);

        obstacles[i].id = 0;
        obstacles[i].type.id = 0;
        obstacles[i].type.health = 0;
        obstacles[i].pos.x = 0;
        obstacles[i].pos.y = 0;

    }

}