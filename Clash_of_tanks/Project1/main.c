#include "game.h"
#include "simpleSdlWrapper/graphics.h"
#include "simpleSdlWrapper/ebentoak.h"
#include "simpleSdlWrapper/soinua.h"
#include "simpleSdlWrapper/text.h"
#include "simpleSdlWrapper/imagen.h"
#include <stdio.h>

#define ENTRENAMENDUA 1
#define VERSUS 2

#define SOINU_KOP 5

int main(int argc, char* str[]) {

    int jarraitu = 0, egoera;

    if (sgHasieratu() == -1) {

        fprintf(stderr, "Unable to set 640x480 video: %s\n", SDL_GetError());

        return 1;

    }

    do {

        hasi();
        egoera = jokoaAurkeztu();
        jarraitu = amaiera(egoera);

    } while (jarraitu);

    sgItxi();

    return 0;

}

