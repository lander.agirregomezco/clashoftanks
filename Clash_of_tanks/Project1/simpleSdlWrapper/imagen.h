#ifndef IMAGEN_H
#define IMAGEN_H

#define MAX_IMG 100

typedef struct Img
{
	int id;
	SDL_Surface* imagen;
	SDL_Texture* texture;
	SDL_Rect dest;

}IMG;

IMG irudiak[MAX_IMG];

extern int irudiKop;

/**
 * irudiaKargatu
 *  *Prepares image for sdl to display it on the window, 
 *  *creating the texture, surface and adding and id to 
 *  *keep track of it
 *  
 *  *Input: Image file name (BMP format!)
 *  
 *  *Output: Image ID
 */

int  irudiaKargatu(char *fileName);

/**
 *    irudiaMugitu
 *    Sets coordinates X & Y to image already prepared 
 *    by 'irudiaKargatu' function
 *
 *   Input: Image ID, X & Y coordinates
 */


void  irudiaMugitu(int numImg, int x, int y);


void irudiakMarraztu(void);
void irudiaKendu(int id);

#endif
